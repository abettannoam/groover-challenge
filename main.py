

def has_sum_subset_equal_to_target(tracks: list[int], target: int, delta: int):
    tracks.sort()
    for i in range(len(tracks)):
        for j in range(i+1, len(tracks)):
            for k in range(j+1, len(tracks)):
                duration_tuple = (tracks[i], tracks[j], tracks[k])
                if target-delta <= sum(duration_tuple) <= target+delta:
                    yield duration_tuple, sum(duration_tuple)



durations = [4, 3, 2, 1, 5, 6]

print('Base challenge')
for sub in has_sum_subset_equal_to_target(durations, target=10, delta=0): print(sub)

print()
print('Part 2')
for sub in has_sum_subset_equal_to_target(durations, target=10, delta=2): print(sub)
