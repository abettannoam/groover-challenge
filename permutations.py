
tracks = ['A', 'B', 'C', 'D']

def permutations(s):
    if len(s) <=1: return [s]
    perms = []
    for e in permutations(s[:-1]):
        for i in range(len(e)+1):
            perms.append(e[:i] + [s[-1]] +e[i:])
    return perms


for perm in permutations(tracks):
    print(perm)
